<?php

/**
 * @file
 * Provide views extender functions.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_extender_views_data_alter(array &$data) {
  $data['views']['entity_field_compare'] = [
    'title' => t('Compare Entity Field with Token'),
    'help' => t('Allow compare entity field data with value from token.'),
    'filter' => [
      'id' => 'entity_field_compare',
    ],
  ];

}
