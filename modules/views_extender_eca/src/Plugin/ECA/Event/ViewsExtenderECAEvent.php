<?php

namespace Drupal\views_extender_eca\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\views_extender_eca\Events\Events;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;

/**
 * Plugin implementation of the ECA Events for config.
 *
 * @EcaEvent(
 *   id = "views_extender_eca",
 *   deriver = "Drupal\views_extender_eca\Plugin\ECA\Event\ViewsExtenderECAEventDeriver"
 * )
 */
class ViewsExtenderECAEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'callback' => [
        'label'       => 'Views Extender Callback',
        'event_name'  => Events::CALL_BACK,
        'event_class' => ViewsExtenderEvent::class,
        'tags'        => Tag::WRITE | Tag::READ,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    if ($this->eventClass() === ViewsExtenderEvent::class) {
      $form['help'] = [
        '#type'        => 'markup',
        '#markup'      => $this->t('This event provides three tokens: <em>"[view_extender_key]"</em> the key to identify, <em>"[view_extender_view_id]"</em> the view id and <em>"[view_extender_config:*]"</em> the config send to event.'),
        '#weight'      => 10,
        '#description' => $this->t('This event provides three tokens: <em>"[view_extender_key]"</em> the key to identify, <em>"[view_extender_view_id]"</em> the view id and <em>"[view_extender_config:*]"</em> the config send to event.'),
      ];
    }
    return $form;
  }

}
