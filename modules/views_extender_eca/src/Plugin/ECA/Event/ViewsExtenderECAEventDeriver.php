<?php

namespace Drupal\views_extender_eca\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for views_extender_eca event plugins.
 */
class ViewsExtenderECAEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return ViewsExtenderECAEvent::definitions();
  }

}
