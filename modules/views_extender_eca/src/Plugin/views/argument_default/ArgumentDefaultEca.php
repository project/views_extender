<?php

namespace Drupal\views_extender_eca\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Drupal\views_extender\Plugin\views\ViewPluginTrait;
use Drupal\views_extender_eca\Events\Events;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The current date time argument default handler.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "views_extender_argument_default_eca",
 *   title = @Translation("ECA argument default")
 * )
 */
class ArgumentDefaultEca extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  use ViewPluginTrait;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected ModuleHandler $moduleHandler;

  /**
   * The event disp.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * CurrentDateTime constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandler $moduleHandler, EventDispatcherInterface $eventDispatcher, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
    $this->eventDispatcher = $eventDispatcher;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('event_dispatcher'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['default_value'] = ['default' => ''];
    $options['eca_key'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['eca_key'] = [
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => $this->t('ECA key'),
      '#description'   => $this->t('The key to allow ECA identify and set back value.'),
      '#default_value' => $this->options['eca_key'],
    ];

    $form['default_value'] = [
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => $this->t('Default value'),
      '#description'   => $this->t('The default value when  ECA not return value. This field support token.'),
      '#default_value' => $this->options['default_value'],
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_tree'] = [
        '#type'            => 'container',
        '#theme'           => 'token_tree_link',
        '#token_types'     => 'all',
        '#global_types'    => TRUE,
        '#click_insert'    => TRUE,
        '#recursion_limit' => 3,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    if (empty($this->options['eca_key'])) {
      return '';
    }

    $viewExtenderEvent = new ViewsExtenderEvent($this->view?->id(), $this->options['eca_key']);
    $viewExtenderEvent->setConfig($this->argument->configuration);
    $this->eventDispatcher->dispatch($viewExtenderEvent, Events::CALL_BACK);

    $value = $viewExtenderEvent->getValue();
    if (is_null($value)) {
      $value = $this->token->replace($this->options['default_value'], $this->getTokenData());
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
