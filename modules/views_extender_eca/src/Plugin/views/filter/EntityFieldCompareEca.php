<?php

namespace Drupal\views_extender_eca\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views_extender\Plugin\views\filter\EntityFieldCompare;
use Drupal\views_extender_eca\Events\Events;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;

/**
 * Filter handler which token.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("entity_field_compare_eca")
 */
class EntityFieldCompareEca extends EntityFieldCompare {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['default_value'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => $this->t('ECA key'),
      '#description'   => $this->t('The key to allow ECA identify and set back value.'),
      '#default_value' => $this->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['default_value'] = [
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => $this->t('Default value'),
      '#description'   => $this->t('The default value when  ECA not return value. This field support token.'),
      '#default_value' => $this->options['default_value'],
    ];
    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_tree'] = [
        '#type'            => 'container',
        '#theme'           => 'token_tree_link',
        '#token_types'     => 'all',
        '#global_types'    => TRUE,
        '#click_insert'    => TRUE,
        '#recursion_limit' => 3,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function opEqual($expression) {
    $placeholder = $this->placeholder();
    $operator = $this->operator();

    $viewExtenderEvent = new ViewsExtenderEvent($this->view?->id(), $this->value);
    $viewExtenderEvent->setConfig(['field' => $this->options['fields']]);
    $this->eventDispatcher->dispatch($viewExtenderEvent, Events::CALL_BACK);

    $value = $viewExtenderEvent->getValue();
    if (is_null($value)) {
      $value = $this->token->replace($this->options['default_value'], $this->getTokenData());
    }
    /* @phpstan-ignore-next-line */
    $this->query->addWhereExpression($this->options['group'], "$expression $operator $placeholder", [$placeholder => $value]);
  }

}
