<?php

namespace Drupal\views_extender_eca\Plugin\views\argument_validator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_validator\ArgumentValidatorPluginBase;
use Drupal\views_extender_eca\Events\Events;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;

/**
 * Validates an argument with ECA callback.
 *
 * @ViewsArgumentValidator(
 *   id = "views_extender_eca_argument_validator",
 *   title = @Translation("ECA Argument validator")
 * )
 */
class ArgumentValidatorEca extends ArgumentValidatorPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['transform'] = ['default' => FALSE];
    $options['eca_key'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['transform'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Transform dashes in URL to spaces in term name filter values'),
      '#default_value' => $this->options['transform'],
    ];

    $form['eca_key'] = [
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => $this->t('ECA key'),
      '#description'   => $this->t('The key to allow ECA identify and set back value.'),
      '#default_value' => $this->options['eca_key'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument) {
    if ($this->options['transform']) {
      $argument = str_replace('-', ' ', $argument);
    }

    $viewExtenderEvent = new ViewsExtenderEvent($this->view?->id(), $this->options['eca_key']);
    $viewExtenderEvent->setConfig([
      'argument' => $argument,
    ]);

    \Drupal::service('event_dispatcher')
      ->dispatch($viewExtenderEvent, Events::CALL_BACK);
    $value = $viewExtenderEvent->getValue();
    $this->argument->argument = $value;
    return TRUE;
  }

}
