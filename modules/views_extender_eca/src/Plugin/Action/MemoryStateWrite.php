<?php

namespace Drupal\views_extender_eca\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\views_extender_eca\Service\MemoryState;

/**
 * Action to store arbitrary value to memory store.
 *
 * @Action(
 *   id = "views_extender_memory_state_write",
 *   label = @Translation("Memory state: write"),
 *   description = @Translation("Writes a value into the Drupal memory system by
 *   the given key.")
 * )
 */
class MemoryStateWrite extends ConfigurableActionBase {

  use MemoryStateTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $value = $this->getTokenValue($this->configuration['value']);
    $key = $this->getTokenValue($this->configuration['key'], TRUE);
    MemoryState::register((string) $key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'key'   => '',
      'value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('State key'),
      '#default_value' => $this->configuration['key'],
      '#weight'        => -30,
      '#description'   => $this->t('The key of the Drupal state.'),
    ];
    $form['value'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('The value of the state'),
      '#default_value' => $this->configuration['value'],
      '#weight'        => -20,
      '#description'   => $this->t('The key, where the value is stored into.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['key'] = $form_state->getValue('key');
    $this->configuration['value'] = $form_state->getValue('value');
    parent::submitConfigurationForm($form, $form_state);
  }

}
