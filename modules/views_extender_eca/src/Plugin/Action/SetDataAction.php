<?php

namespace Drupal\views_extender_eca\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;

/**
 * Action to set data callback to view.
 *
 * @Action(
 *   id = "views_extender_eca_set_data",
 *   label = @Translation("Views Extender ECA: set data"),
 *   description = @Translation("Set updated data to view")
 * )
 */
class SetDataAction extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $event = $this->getEvent();
    if (!$event || !($event instanceof ViewsExtenderEvent)) {
      return;
    }
    $token = $this->tokenServices;
    $value = $this->configuration['value'];
    $value = (string) $token->replaceClear($value);
    $event->setValue($value);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['value'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('The data callback for view.'),
      '#default_value' => $this->configuration['value'],
      '#weight'        => -20,
      '#description'   => $this->t('Set the callback value data for view.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['value'] = $form_state->getValue('value');
    parent::submitConfigurationForm($form, $form_state);
  }

}
