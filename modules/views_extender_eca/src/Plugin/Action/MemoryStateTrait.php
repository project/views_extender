<?php

namespace Drupal\views_extender_eca\Plugin\Action;

use Drupal\eca\Plugin\DataType\DataTransferObject;
use Drupal\eca\Token\TokenInterface;

/**
 * The Memory State Trait.
 *
 * @package Drupal\views_extender_eca\Plugin\Action
 */
trait MemoryStateTrait {

  /**
   * The ECA-related token services.
   *
   * @var \Drupal\eca\Token\TokenInterface
   */
  protected TokenInterface $tokenServices;

  /**
   * Get token value.
   *
   * @param string $text
   *   The input text.
   * @param bool $reset
   *   Is reset if value is array.
   *
   * @return mixed
   *   The result.
   */
  public function getTokenValue(string $text, bool $reset = FALSE): mixed {
    $value = $this->tokenServices->getOrReplace($text);
    if ($value instanceof DataTransferObject) {
      $value = $value->getValue();
    }
    if ($reset) {
      if (is_scalar($value) || (is_object($value) && method_exists($value, '__toString'))) {
        $value = (string) $value;
      }

      if (is_array($value)) {
        $value = reset($value);
      }
    }

    return $value;
  }

}
