<?php

namespace Drupal\views_extender_eca\Service;

/**
 * The Memory State for Views Extender ECA.
 *
 * @package Drupal\views_extender_eca\Service
 */
class MemoryState {

  /**
   * Registry storage.
   *
   * @var array
   */
  static private array $registry = [];

  /**
   * Register a new variable.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   * @param bool $override
   *   The override flag.
   */
  public static function register($key, $value, $override = TRUE) {
    if (isset(self::$registry[$key])) {
      if (!$override) {
        return;
      }
    }
    self::$registry[$key] = $value;
  }

  /**
   * Unregister a variable from register by key.
   *
   * @param string $key
   *   The key.
   */
  public static function unregister($key) {
    if (isset(self::$registry[$key])) {
      if (is_object(self::$registry[$key]) && (method_exists(self::$registry[$key], '__destruct'))) {
        self::$registry[$key]->__destruct();
      }
      unset(self::$registry[$key]);
    }
  }

  /**
   * Retrieve a value from registry by a key.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   Register result.
   */
  public static function registry($key) {
    if (isset(self::$registry[$key])) {
      return self::$registry[$key];
    }
    return NULL;
  }

}
