<?php

namespace Drupal\views_extender_eca\EventSubscriber;

use Drupal\eca\EcaEvents;
use Drupal\eca\Event\BeforeInitialExecutionEvent;
use Drupal\eca\EventSubscriber\EcaBase;
use Drupal\views_extender_eca\Events\ViewsExtenderEvent;

/**
 * Adds the configuration to the Token service when executing ECA logic.
 */
class EcaExecutionViewExtenderSubscriber extends EcaBase {

  /**
   * Subscriber method before initial execution.
   *
   * @param \Drupal\eca\Event\BeforeInitialExecutionEvent $before_event
   *   The according event.
   */
  public function onBeforeInitialExecution(BeforeInitialExecutionEvent $before_event): void {
    $event = $before_event->getEvent();
    if ($event instanceof ViewsExtenderEvent) {
      $this->tokenService->addTokenData('view_extender_view_id', $event->getViewId());
      $this->tokenService->addTokenData('view_extender_key', $event->getKey());
      $this->tokenService->addTokenData('view_extender_config', $event->getConfig());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[EcaEvents::BEFORE_INITIAL_EXECUTION][] = ['onBeforeInitialExecution'];
    return $events;
  }

}
