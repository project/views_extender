<?php

namespace Drupal\views_extender_eca\EventSubscriber;

use Drupal\eca\EventSubscriber\EcaBase;
use Drupal\views_extender_eca\Plugin\ECA\Event\ViewsExtenderECAEvent;

/**
 * ECA config event subscriber.
 */
class EcaViewsExtender extends EcaBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    foreach (ViewsExtenderECAEvent::definitions() as $definition) {
      $priority = 0;
      $events[$definition['event_name']][] = ['onEvent', $priority];
    }
    return $events;
  }

}
