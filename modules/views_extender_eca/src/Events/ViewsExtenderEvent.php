<?php

namespace Drupal\views_extender_eca\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * The view extender event.
 *
 * @package Drupal\views_extender_eca\Events
 */
class ViewsExtenderEvent extends Event {

  /**
   * The view id.
   *
   * @var string|null
   */
  protected ?string $viewId;

  /**
   * The key data.
   *
   * @var string
   */
  protected string $key;

  /**
   * The value data.
   *
   * @var mixed
   */
  protected mixed $value;

  /**
   * Storage the config.
   *
   * @var array
   */
  protected array $config = [];

  /**
   * ViewsExtenderEvent constructor.
   *
   * @param string|null $view_id
   *   The view id.
   * @param string $key
   *   The key data.
   */
  public function __construct(?string $view_id, string $key = '') {
    $this->viewId = $view_id;
    $this->key = $key;
  }

  /**
   * The config.
   *
   * @param array $configuration
   *   The configuration.
   */
  public function setConfig(array $configuration) {
    $this->config = $configuration;
  }

  /**
   * Get the config.
   *
   * @return array
   *   The config.
   */
  public function getConfig():array {
    return $this->config;
  }

  /**
   * Get the view id.
   *
   * @return string|null
   *   The view id.
   */
  public function getViewId(): ?string {
    return $this->viewId;
  }

  /**
   * Get the key data.
   *
   * @return string
   *   The key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Set value data.
   *
   * @param mixed $data
   *   The value.
   */
  public function setValue(mixed $data) {
    $this->value = $data;
  }

  /**
   * Get the value data.
   *
   * @return mixed
   *   The return value.
   */
  public function getValue(): mixed {
    return $this->value;
  }

}
