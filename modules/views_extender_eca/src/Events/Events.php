<?php

namespace Drupal\views_extender_eca\Events;

/**
 * Events name for Views Extender.
 *
 * @package Drupal\views_extender_eca\Events
 */
class Events {

  const CALL_BACK = 'views_extender.callback';

}
