<?php

/**
 * @file
 * Provide views extender functions.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_extender_eca_views_data_alter(array &$data) {
  $data['views']['entity_field_compare_eca'] = [
    'title'  => t('Compare Entity Field with ECA'),
    'help'   => t('Allow compare entity field data with value from ECA.'),
    'filter' => [
      'id' => 'entity_field_compare_eca',
    ],
  ];

}
