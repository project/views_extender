# Views Extender for Drupal

## Argument Default

- Current Date Time as default argument.

## Argument Validator

- Taxonomy term field as ID.
- Taxonomy term alias as ID.

## Filter

- Entity field compare with Token
- Entity field compare with ECA: https://www.drupal.org/project/eca
