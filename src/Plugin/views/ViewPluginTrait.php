<?php

namespace Drupal\views_extender\Plugin\views;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * The View Plugin Trait.
 *
 * @package Drupal\views_extender\Plugin\views
 */
trait ViewPluginTrait {

  /**
   * Get entity storage instance.
   *
   * @param string $key
   *   The storage key name.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface|null
   *   The storage instance or null.
   */
  protected function getEntityStorage(string $key): ?EntityStorageInterface {
    try {
      return \Drupal::entityTypeManager()->getStorage($key);
    }
    catch (\Exception $exception) {
      return NULL;
    }
  }

  /**
   * Get token data.
   *
   * @return array
   *   The token data.
   */
  protected function getTokenData(): array {
    $token_data = [];
    $entityManager = \Drupal::entityTypeManager();
    $params = \Drupal::routeMatch()->getParameters();
    foreach ($params->keys() as $key) {
      $param_object = $params->get($key);
      if ($key === 'node' && !is_object($param_object)) {
        $param_object = Node::load($param_object);
      }
      if ($key === 'user' && !is_object($param_object)) {
        $param_object = User::load($param_object);
      }
      if ($key === 'node_revision' && !is_object($param_object)) {
        $param_object = $entityManager
          ->getStorage('node')
          ->loadRevision($param_object);
      }
      if (!is_object($param_object) && $storage = $this->getEntityStorage($key)) {
        $param_object = $storage->load($param_object);
      }
      $token_data[$key] = $param_object;
    }

    return $token_data;
  }

}
