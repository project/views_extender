<?php

namespace Drupal\views_extender\Plugin\views\argument_validator;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\argument_validator\Entity;

/**
 * Validates whether a term alias is a valid term argument.
 *
 * @ViewsArgumentValidator(
 *   id = "taxonomy_term_alias",
 *   title = @Translation("Taxonomy term alias"),
 *   entity_type = "taxonomy_term"
 * )
 */
class TermAlias extends Entity {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Path alias.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $aliasStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_type_bundle_info);
    // Not handling exploding term names.
    $this->multipleCapable = FALSE;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->aliasStorage = $entity_type_manager->getStorage('path_alias');
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument) {
    $alias = $this->aliasStorage->loadByProperties(['alias' => '/' . $argument]);
    if (!$alias) {
      // Returned empty array no alias with the name.
      return FALSE;
    }
    /** @var \Drupal\path_alias\Entity\PathAlias $alias */
    $alias = reset($alias);
    $path = $alias->get('path')->value;
    if (str_contains($path, '/taxonomy/term/')) {
      $term_id = str_replace('/taxonomy/term/', '', $path);
      $term = $this->termStorage->load($term_id);
      if (!$term || !$this->validateEntity($term)) {
        return FALSE;
      }
      $this->argument->argument = $term_id;
      return TRUE;
    }

    return FALSE;
  }

}
