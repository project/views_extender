<?php

namespace Drupal\views_extender\Plugin\views\argument_validator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Plugin\views\argument_validator\TermName;

/**
 * Validates an argument as a term name and converts it to the term ID.
 *
 * @ViewsArgumentValidator(
 *   id = "taxonomy_term_field_into_id",
 *   title = @Translation("Taxonomy term field as ID"),
 *   entity_type = "taxonomy_term"
 * )
 */
class TermFieldAsId extends TermName {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['field'] = ['default' => FALSE];
    $options['field_prefix'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['field'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Field name'),
      '#description' => $this->t('Input field name for validator'),
      '#default_value' => $this->options['field'],
    ];

    $form['field_prefix'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Field prefix'),
      '#description' => $this->t('Field prefix for argument, Example for url compare need "/" prefix'),
      '#default_value' => $this->options['field_prefix'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument) {
    if ($this->options['transform']) {
      $argument = str_replace('-', ' ', $argument);
    }
    $terms = [];
    // If bundles is set then restrict the loaded terms to the given bundles.
    if (!empty($this->options['bundles'])) {
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', $this->options['bundles']);
      $query->condition('status', 1);
      $query->condition($this->options['field'], $this->options['field_prefix'] . rtrim($argument, '/'));
      // Limit only 10 term.
      $query->range(0, 10);
      $results = $query->execute();
      if (!empty($results)) {
        $terms = Term::loadMultiple($results);
      }
    }

    // $terms are already bundle tested but we need to test access control.
    foreach ($terms as $term) {
      if ($this->validateEntity($term)) {
        // We only need one of the terms to be valid, so set the argument to
        // the term ID return TRUE when we find one.
        $this->argument->argument = $term->id();
        return TRUE;
        // @todo If there are other values in $terms, maybe it'd be nice to
        // warn someone that there were multiple matches and we're only using
        // the first one.
      }
    }
    return FALSE;
  }

}
