<?php

namespace Drupal\views_extender\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views_extender\Plugin\views\ViewPluginTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Filter handler which token.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("entity_field_compare")
 */
class EntityFieldCompare extends StringFilter {

  use ViewPluginTrait;

  /**
   * Constant for event name.
   */
  const ENTITY_FIELD_COMPARE = 'views_extender_entity_field_compare';

  /**
   * Where the $query object will reside.
   *
   * @var \Drupal\views\Plugin\views\query\QueryPluginBase
   */
  public $query;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * Constructs a new StringFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   The account proxy.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, AccountProxyInterface $account_proxy, EventDispatcherInterface $event_dispatcher, RouteMatchInterface $route_match, RequestStack $request_stack, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $connection);
    $this->accountProxy = $account_proxy;
    $this->eventDispatcher = $event_dispatcher;
    $this->routeMatch = $route_match;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('current_user'),
      $container->get('event_dispatcher'),
      $container->get('current_route_match'),
      $container->get('request_stack'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'opEqual',
        'short' => $this->t('<'),
        'values' => 1,
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'opEqual',
        'short' => $this->t('<='),
        'values' => 1,
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'opEqual',
        'short' => $this->t('='),
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'opEqual',
        'short' => $this->t('!='),
        'values' => 1,
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'opEqual',
        'short' => $this->t('>='),
        'values' => 1,
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'opEqual',
        'short' => $this->t('>'),
        'values' => 1,
      ],
      'contains' => [
        'title' => $this->t('Contains'),
        'short' => $this->t('contains'),
        'method' => 'opContains',
        'values' => 1,
      ],
      'not' => [
        'title' => $this->t('Does not contain'),
        'short' => $this->t('!has'),
        'method' => 'opNotLike',
        'values' => 1,
      ],
    ];
    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['fields'] = ['default' => NULL];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function showValueForm(&$form, FormStateInterface $form_state) {
    $this->valueForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#prefix' => '<div class="clear"></div>',
      '#type' => 'textfield',
      '#title' => $this->t('Token Value'),
      '#size' => 30,
      '#default_value' => $this->value,
    ];
    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_tree'] = [
        '#type' => 'container',
        '#theme' => 'token_tree_link',
        '#token_types' => 'all',
        '#global_types' => TRUE,
        '#click_insert' => TRUE,
        '#recursion_limit' => 3,
      ];
    }
  }

  /**
   * Get field options.
   *
   * @return array
   *   Get all field options.
   */
  protected function getFieldOptions(): array {
    $options = [];
    foreach ($this->view->display_handler->getHandlers('field') as $name => $field) {
      $options[$name] = $field->adminLabel(TRUE);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $this->view->initStyle();

    // Allow to choose all fields as possible.
    if ($this->view->style_plugin->usesFields()) {
      $options = $this->getFieldOptions();
      if ($options) {
        $form['fields'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose field to compare'),
          '#description' => $this->t("Select field to compare with user field."),
          '#multiple' => FALSE,
          '#options' => $options,
          '#default_value' => $this->options['fields'],
          '#weight' => -1,
        ];
      }
      else {
        $form_state->setErrorByName('', $this->t('You have to add some fields to be able to use this filter.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $options = $this->getFieldOptions();
    $left = $options[$this->options['fields']] ?? $this->options['fields'];
    $right = $this->value;

    return $left . ' ' . $this->operator . ' ' . $right;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->view->_build('field');
    $fields = [];
    $id = $this->options['fields'];

    if (!isset($this->view->field[$id])) {
      $this->view->build_info['fail'] = TRUE;
      return;
    }
    $field = $this->view->field[$id];
    // Always add the table of the selected fields to be sure
    // a table alias exists.
    $field->ensureMyTable();
    if (!empty($field->field_alias) && !empty($field->field_alias)) {
      $fields[] = "$field->tableAlias.$field->realField";
    }
    if ($fields) {
      $expression = reset($fields);
      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        $this->{$info[$this->operator]['method']}($expression);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    if ($this->displayHandler->usesFields()) {
      $fields = $this->displayHandler->getHandlers('field');
      $id = $this->options['fields'];
      if (!isset($fields[$id])) {
        $errors[] = $this->t('Field %field set in %filter is not set in display %display.', [
          '%field' => $id,
          '%filter' => $this->adminLabel(),
          '%display' => $this->displayHandler->display['display_title'],
        ]);
      }
    }
    else {
      $errors[] = $this->t('%display: %filter can only be used on displays that use fields. Set the style or row format for that display to one using fields to use the combine field filter.', [
        '%display' => $this->displayHandler->display['display_title'],
        '%filter' => $this->adminLabel(),
      ]);
    }
    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function operator() {
    return $this->operator;
  }

  /**
   * {@inheritdoc}
   */
  public function opEqual($expression) {
    $placeholder = $this->placeholder();
    $operator = $this->operator();
    $value = $this->token->replace($this->value, $this->getTokenData());
    if ($value !== NULL) {
      /* @phpstan-ignore-next-line */
      $this->query->addWhereExpression($this->options['group'], "$expression $operator $placeholder", [$placeholder => $value]);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opContains($field) {
    $value = $this->token->replace($this->value, $this->getTokenData());
    $value = $this->connection->escapeLike($value);
    $operator = $this->getConditionOperator('LIKE');
    $this->query->addWhere($this->options['group'], $field, '%' . $value . '%', $operator);
  }

  /**
   * {@inheritdoc}
   */
  protected function opNotLike($field) {
    $value = $this->token->replace($this->value, $this->getTokenData());
    $value = $this->connection->escapeLike($value);
    $operator = $this->getConditionOperator('NOT LIKE');
    $this->query->addWhere($this->options['group'], $field, '%' . $value . '%', $operator);
  }

}
